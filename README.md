# Lambda DynamoDB Prediction Requester #

Lambda python scripts: DynamoDB, Kinesis Stream & Kinesis Firehose. They perform real-time feature engineering, by adding extra features that have positive values (important for the sequential Keras LSTM model input).


## Technologies
- Internet Of Things (AWS IoT)
- NoSQL (AWS DynamoDB) storage & streams
- AWS IoT
- AWS Lambda
- AWS S3
- AWS Kinesis Streams
- AWS Kinesis Firehose


## SDKs & Libraries

- json, re
- requests
- base64
