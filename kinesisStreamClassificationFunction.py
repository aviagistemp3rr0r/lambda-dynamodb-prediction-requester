import base64
import json
import requests

url = 'http://52.208.37.243:5000/predict'
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

def handler(event, context):
    for record in event['Records']:
        payload = json.loads(base64.b64decode(record['kinesis']['data']))
        requests.post(url, data=json.dumps({'classification': int(payload['data']['classification']), \
        'row':  [int(payload['data']['ax']), int(payload['data']['ay']), int(payload['data']['az']), \
        int(payload['data']['gx']), int(payload['data']['gy']), int(payload['data']['gz']), \
        int(payload['data']['ax2']), int(payload['data']['ay2']), int(payload['data']['az2']), \
        int(payload['data']['gx2']), int(payload['data']['gy2']), int(payload['data']['gz2']), \
        int(payload['data']['mx']), int(payload['data']['my']), int(payload['data']['mz']), \
        int(payload['data']['steps']), int(payload['data']['temp'])], 'timestamp': payload['timestamp']}), headers=headers)