import json
import requests

url = 'http://52.208.37.243:5000/predict'
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

def handler(event, context):
    for record in event['Records']:
        if 'NewImage' in record['dynamodb']:
            dataDynamoDB = record['dynamodb']['NewImage']['payload']['M']['data']['M']
            requests.post(url, data=json.dumps({'classification': int(dataDynamoDB['classification']['N']), \
			'row': [int(dataDynamoDB['ax']['N']), int(dataDynamoDB['ay']['N']), int(dataDynamoDB['az']['N']), \
			int(dataDynamoDB['gx']['N']), int(dataDynamoDB['gy']['N']), int(dataDynamoDB['gz']['N']), \
			int(dataDynamoDB['ax2']['N']), int(dataDynamoDB['ay2']['N']), int(dataDynamoDB['az2']['N']), \
			int(dataDynamoDB['gx2']['N']), int(dataDynamoDB['gy2']['N']), int(dataDynamoDB['gz2']['N']), \
			int(dataDynamoDB['mx']['N']), int(dataDynamoDB['my']['N']), int(dataDynamoDB['mz']['N']), \
			int(dataDynamoDB['steps']['N']), int(dataDynamoDB['temp']['N'])], \
			'timestamp': record['dynamodb']['NewImage']['timestamp']['N']}), headers=headers)