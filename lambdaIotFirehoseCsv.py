#from __future__ import print_function
import base64
import re
import json
#print('Loading function')
def lambda_handler(event, context):
    output = []
    succeeded_record_cnt = 0
    failed_record_cnt = 0
    for record in event['records']:
        #print("Firehose from IoT Data id: " + record['recordId'])
        payload = json.loads(base64.b64decode(record['data']))
        #print("Firehose from IoT Data payload: " + str(payload))
        succeeded_record_cnt += 1
        row = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        row[0] = - 1 * int(payload['data']['ax'])
        row[1] = - 1 * int(payload['data']['ay'])
        row[2] = - 1 * int(payload['data']['az'])
        row[3] = - 1 * int(payload['data']['gx'])
        row[4] = - 1 * int(payload['data']['gy'])
        row[5] = - 1 * int(payload['data']['gz'])		
		
        row[6] = - 1 * int(payload['data']['ax2'])
        row[7] = - 1 * int(payload['data']['ay2'])
        row[8] = - 1 * int(payload['data']['az2'])
        row[9] = - 1 * int(payload['data']['gx2'])
        row[10] = - 1 * int(payload['data']['gy2'])
        row[11] = - 1 * int(payload['data']['gz2'])		
        row[12] = - 1 * int(payload['data']['mx'])
        row[13] = - 1 * int(payload['data']['my'])
        row[14] = - 1 * int(payload['data']['mz'])
		
        row[15] = - 1 * int(payload['data']['steps'])
        row[16] = - 1 * int(payload['data']['temp'])
		
        for i in range(len(row)):
            row.append(-1 * row[i])
        for i in range(len(row)):
            if row[i] < 0:
                row[i] = 0
        output_payload = str(payload['timestamp']) + ',' + str(payload['data']['classification']) + ',' + \
		str(row[0]) + ',' + str(row[1]) + ',' + str(row[2]) + ',' + str(row[3]) + ',' \
		+ str(row[4]) + ',' + str(row[5]) + ',' + str(row[6]) + ',' + str(row[7]) + ',' \
		+ str(row[8]) + ',' + str(row[9]) + ',' + str(row[10]) + ',' + str(row[11]) + ',' \
		+ str(row[12]) + ',' + str(row[13]) + ',' + str(row[14]) + ',' + str(row[15]) + ',' \
		+ str(row[16]) + ',' + str(row[17]) + ',' + str(row[18]) + ',' + str(row[19]) + ',' \
		+ str(row[20]) + ',' + str(row[21]) + ',' + str(row[22]) + ',' + str(row[23]) + ',' \
		+ str(row[24]) + ',' + str(row[25]) + ',' + str(row[26]) + ',' + str(row[27]) + ',' \
		+ str(row[28]) + ',' + str(row[29]) + ',' + str(row[30]) + ',' + str(row[31]) + ',' \
		+ str(row[32]) + ',' + str(row[33]) + '\n'
        output_record = {
            'recordId': record['recordId'],
            'result': 'Ok',
            'data': base64.b64encode(output_payload)
        }
        output.append(output_record)
        #print('Processing completed.  Successful records {}, Failed records {}.'.format(succeeded_record_cnt, failed_record_cnt))
    return {'records': output}
